#!/usr/bin/env python3
import argparse
import datetime
import enum
import html
import json
import logging
import os.path
import re
import sys

import commonmark

_logger = logging.getLogger(__name__)
this_dir = os.path.dirname(__file__)
show_diffs = False

def diff_text(a, b):
    '''Debug utility for comparing two strings.'''
    if not show_diffs:
        return
    import tempfile
    import os
    import subprocess
    name1, name2 = None, None
    try:
        with tempfile.NamedTemporaryFile('w', delete=False) as f1:
            name1 = f1.name
            f1.write(a)
        with tempfile.NamedTemporaryFile('w', delete=False) as f2:
            name2 = f2.name
            f2.write(b)
        subprocess.call(['git', '-P', 'diff',
                         # '--color-words',
                         f1.name, f2.name],
                        close_fds=False, stdout=sys.stderr)
    finally:
        for name in (name1, name2):
            if name is not None:
                os.unlink(name)


def parse_comments(obj):
    keys = 'attachment_id bug_id count creation_time creator id is_private tags text time'.split()
    for bug in obj['bugs'].values():
        for comment in bug['comments']:
            info = {k: comment[k] for k in keys}
            yield info
    """ Example comment:
        {
          "attachment_id": null,
          "bug_id": 1,
          "count": 0,
          "creation_time": "2005-04-06T14:47:50Z",
          "creator": "ulf.lamping@web.de",
          "id": 1,
          "is_private": false,
          "tags": [],
          "text": "I'll find the bug later :-)",
          "time": "2005-04-06T14:47:50Z"
        },
    """


def bug_link(comment):
    return 'https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=%d#c%d' % \
        (comment['bug_id'], comment['count'])


class Category(enum.Enum):
    '''Classifications of the text conversion result.'''
    nospecial = enum.auto()
    samehtml = enum.auto()
    unprocessed = enum.auto()


thematic_break_re = re.compile('^(\s{,3})((-\s*){3,})\s*$', re.MULTILINE)
# commonmark.py searches for setext headings with r'^(?:=+|-+)[ \t]*$',
# which is more loose than the official spec.
setext_re = re.compile('^(\s{,3})(-{3,})', re.MULTILINE)
# Special GitLab references / autolinks
# https://docs.gitlab.com/ee/user/markdown.html#special-gitlab-references
special_reference_re = re.compile('([@#!\$&~%])(\w)')
def sanitize(text, warn=True):
    # Identify dangerous markdown characters and escape them.
    # https://spec.commonmark.org/0.29/#ascii-punctuation-character
    punct = r'!"#$%&' + r"()*+,-./:;<=>?@[\]^_`{|}~"
    # Some chars are actually not that bad.
    badchars = ''.join(x for x in punct if x not in '-.')
    trans = str.maketrans({k: '\\' + k for k in badchars})
    res = text.translate(trans)
    # Thematic breaks (<hr>) and dashed setext headings (<h2>)
    res = thematic_break_re.sub(r'\1\\\2', res)
    res = setext_re.sub(r'\1\\\2', res)
    # Insert a zero-width joiner between special reference signifiers and numbers or letters.
    res = special_reference_re.sub(r'\1&#8205;\2', res)
    if warn and res != text:
        _logger.debug('Sanitized %d -> %d %r', len(text), len(res), res)
    return res


def fix_blockquote(text):
    '''
    Converts something such as:

        (In reply to Bob from comment #2)
        > (In reply to Alice from comment #1)
        > > First
        > Second
        Third.

    to

        (In reply to Bob from comment #2)
        > (In reply to Alice from comment #1)
        > > First
        >
        > Second

        Third.

    Returns a tuple with the above fixed source as well as the HTML that this
    "Markdown source" should have been converted into.
    '''
    lines = text.splitlines()
    lines_out = []
    lines_out_html = []
    depth = 0
    for line in lines:
        if re.match('^\s*(\(In reply to|>)', line):
            line += '  '
        prefixlen = len(re.match(r'^(> *)*', line).group())
        prefix, tail = line[:prefixlen], line[prefixlen:]
        current_depth = len(prefix.replace(' ', ''))
        if current_depth > depth:
            lines_out_html.extend(['<blockquote>'] * (current_depth - depth))
            depth = current_depth
        elif current_depth < depth:
            # Insert an extra indentation if the current line is non-empty.
            if tail.strip():
                lines_out.append(prefix.rstrip())
            lines_out_html.extend(['</blockquote>'] * -(current_depth - depth))
            depth = current_depth
        lines_out.append(line)
        lines_out_html.append(tail)
    if depth > 0:
        lines_out_html.extend(['</blockquote>'] * depth)
    return '\n'.join(lines_out), '\n'.join(lines_out_html)


# Wireshark's Bugzilla instance has a custom field for build information.
# Search for it and if found, return a CommonMark-formatted version and
# the remaining text, else return None, and the original text.
build_info_re = re.compile('^Build Information', re.IGNORECASE|re.MULTILINE)
trailing_nl_re = re.compile('(and contributors\.|gpl-2.0.html>)\nThis is free software')
separator_re = re.compile('^--$', re.MULTILINE)
def split_build_information(text):
    if not build_info_re.search(text):
        return (None, text)

    try:
        raw_bi, remainder = separator_re.split(text, maxsplit=1)
    except ValueError:
        return (None, text)

    cm_lines = []
    trailing_nl = False
    # Our pasted content has trailing newlines if we have a \n before "This
    # is free software" and probably does if we have a bunch of newlines.
    if trailing_nl_re.search(raw_bi) or raw_bi.count('\n') > 10:
        trailing_nl = True
    bi_split = '\n\n' if trailing_nl else '\n'

    for line in raw_bi.split(bi_split):
        s_line = sanitize(line, False)
        if trailing_nl:
            s_line.replace('\n', ' ')
        if build_info_re.search(line):
            line = f'**{s_line}**'
        else:
            line = s_line
        cm_lines.append(s_line + '\n')

    cm_bi = '\n'.join(cm_lines)
    return (cm_bi, remainder)


def join_build_information(build_info_block, text):
    if build_info_block is None:
        return text
    return build_info_block + '\n' + text


# Autolink "bug 1000"
bug_link_re = re.compile('(bug\s*)(\d+)', re.IGNORECASE|re.MULTILINE)
def add_bug_links(text):
    return bug_link_re.sub(r'\1#\2', text)


# <a href="mailto:foo@example.org">foo@example.org</a>
# <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html">http://www.gnu.org/licenses/old-licenses/gpl-2.0.html</a>
href_re = re.compile('(<a href="[^"]+">([^<]+)</a>)')

def convert_text(text):
    # Remove trailing whitespace first, they have a special meaning in Markdown.
    text = re.sub(r'[\t ]+\n', '\n', text)
    # Remove leading and trailing newlines, they are useless.
    text = text.strip('\n')

    # Get a pretty version of the build information, separate from the
    # rest of our comment.
    (cm_build_info, text) = split_build_information(text)

    if sanitize(text, False) == text:
        # No special characters at all, nothing to do.
        text = add_bug_links(text)
        return join_build_information(cm_build_info, text), Category.nospecial

    # Hack: fixup known bad leading trailing whitespace.
    text = re.sub(r'^ (\(In reply to comment)', r'\1', text, flags=re.M)

    # TODO check for characters such as '#' and '!' which are special in GFM.
    # TODO recognize '(In reply to comment #0)' and strip '#'.
    # TODO recognize bug links and commit references.

    # Ensure '>' quotes have an empty line after it "ends".
    text, text_with_blockquote_html = fix_blockquote(text)

    # Lots of newlines implies pasted-in code.
    looks_like_code = text.count('\n') > 10

    # Quick sanity check whether the generated HTML would differ.
    out = commonmark.commonmark(text)
    out = out.replace('<p>', '\n').strip('\n')
    out = out.replace('</p>', '')
    out = out.replace('<blockquote>\n\n', '<blockquote>\n')
    out = out.replace('\n<blockquote>', '  \n<blockquote>')
    out = out.replace('\n</blockquote>', '  \n</blockquote>')
    out = out.replace('<br />\n', '  \n')
    out = out.replace('(<code>|</code>', '`')
    # TODO should <em> be converted to `*`? We may not know for sure whether `*`
    # was intended to imply that, it could be the output from something else.
    out = html.unescape(out)
    if out == text_with_blockquote_html and not looks_like_code:
        text = sanitize(text)
        text = add_bug_links(text)
        return join_build_information(cm_build_info, text), Category.samehtml

    # We ran into something which we do not know how to process yet.
    # TODO remove this diff_text debug tool after development.
    diff_text(text_with_blockquote_html, out)

    # Fallback for possibly dangerous comments. Note: URLs and everything gets
    # lost. We should probably try to preserve URLs and other formatting.
    text = '\n' + re.sub('^', '    ', text, flags=re.M)
    return join_build_information(cm_build_info, text), Category.unprocessed


def make_note(comment):
    keys = 'attachment_id count creator creation_time is_private time'.split()
    note = {f'bz_{k}': comment[k] for k in keys}
    # note['user'] = sanitize(comment['creator'].split('@')[0])
    note['link'] = bug_link(comment)
    text, classification = convert_text(comment['text'].rstrip())
    assert classification in Category
    note['body'] = text + '\n'
    return note, classification


def write_notes(note_dir, bug_id, notes):
    with open(os.path.join(note_dir, f'issue{bug_id}-notes.json'), 'w') as in_f:
        json.dump(notes, in_f, indent=2)

parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true',
                    help='Enable debug logging')
#parser.add_argument('--bzurl', default='https://bugs.wireshark.org/bugzilla/')
parser.add_argument('--limit', default='all',
                    help='Comment classifications to limit to. Defaults to %(default)r. Use "help" to list all options.')
parser.add_argument('--oneline', action='store_true', help='Compact output')
parser.add_argument('--show-diffs', action='store_true', help='Show markdown round-trip conversion diffs')
parser.add_argument('--note-dir', default=os.path.join(this_dir, 'notes'),
                    help='Directory containing the output of fetch_bugs.py (default: <script_dir>/notes)')
parser.add_argument('files', nargs='+',
                    help='bugX-comment.json files from fetch_bugs.py')


def parse_limits(query):
    if query == 'help':
        print('Available categories:', file=sys.stderr)
        for cat in Category:
            print(f'  {cat.name}', file=sys.stderr)
        return None
    limits = set()
    for val in query.split(','):
        if val == '':
            continue
        elif val.startswith('-'):
            limits.discard(Category[val[1:]])
        elif val.startswith('+'):
            limits.add(Category[val[1:]])
        else:
            limits.clear()
            if val == 'all':
                limits.update(Category)
            else:
                limits.add(Category[val])
    return limits


def main():
    args = parser.parse_args()
    global show_diffs
    show_diffs = args.show_diffs
    note_dir = args.note_dir
    logging.basicConfig(format='%(asctime)s %(message)s',
                        level=logging.DEBUG if args.debug else logging.INFO)

    limits = parse_limits(args.limit)
    if not limits:
        if limits is not None:
            _logger.warning('Set of categories is empty!')
        return
    _logger.debug('Limiting comments to: %s', ','.join(c.name for c in limits))

    bug_comments = {} # { bug_id: [comment1, comment2, ...] }
    for filename in args.files:
        with open(filename, 'r') as f:
            obj = json.load(f)
            for comment in list(parse_comments(obj)):
                bug_id = comment['bug_id']
                try:
                    bug_comments[bug_id].append(comment)
                except KeyError:
                    bug_comments[bug_id] = [comment]

    bug_ids = sorted(bug_comments.keys())
    for bug_id in bug_ids:
        notes = []
        for comment in bug_comments[bug_id]:
            note, classification = make_note(comment)
            if classification not in limits:
                _logger.debug(f'Unable to convert {bug_id}#{comment["count"]}. Classification: %s', classification)
                continue
            notes.append(note)
        if notes:
            write_notes(note_dir, bug_id, notes)
            notes_count = len(notes)
            _logger.warning(f'Wrote {len(notes)} notes for issue {bug_id}.')
        else:
            _logger.warning(f'No convertible comments in bug {bug_id}.')


if __name__ == '__main__':
    main()
