# Bugzilla to GitLab issues

Some utilities to aid converting the contents of Bugzilla (bugs, comments, maybe
attachments?) to GitLab issues.

## Dependencies

* Python 3.6 or newer
* [requests](https://2.python-requests.org/en/master/)
* [commonmark](https://github.com/readthedocs/commonmark.py)
* [dateutil](https://dateutil.readthedocs.io/en/stable/)
* [python-gitlab](https://github.com/python-gitlab/python-gitlab)

## Scripts

* `fetch_bugs.py` - fetch bugs and their comments, storing them in `bugX.json`
  and `bugX-comment.json` files respectively for the given bug IDs.
* `fetch_attachments.py` - fetch attachments, storing them in `bugX-attachmentY-filename`
  and `bugX-attachmentY.json` files for the givent attachment IDs.
* `convert_comments.py` - given the above `bugX-comment.json` files, produce a
  representation of the comments in Markdown. Right now it will also output a
  diff for comments which are not fully understood yet.

All of these scripts support a `--help` option to learn more about the options.

## Examples

Initialize the labels for the bz-migration-test project using labels.json:

    ./create_labels.py wireshark/bz-migration-test

Retrieve a sample of the bugs and attachments for development:

    mkdir bugs
    ./fetch_bugs.py 1-99 15000-15099 16000-16099
    mkdir attachments
    ./fetch_attachments.py 1-100 15000-15099 17000-17099

Fetch attachments for each of our fetched bugs:

    ./fetch_attachments.py $(sed -n 's/.*attachment_id": \([0-9][0-9]*\),/\1/p' bugs/bug*-comment.json | sort -n )

Upload an attachment to the bz-migration-test project.
Note that the Gitlab API returns a differnt path for the same file on subsequent runs, so this should probably be run *once* for each attachment:

    ./upload_attachments.py wireshark/bz-migration-test attachments/bug11-attachment2-ethereal_Messenger.pcap

Upload all attachments:

    find attachments -name 'bug*-attachment*-*' | xargs -n 500 ./upload_attachments.py geraldcombs/ws-bug-migration-sandbox

Convert comments and limit the output to comments which are not fully understood
yet. Fold a whole comment into a single line in order to improve scanning
through the output.

    mkdir issues
    ./convert_comments.py bugs/bug{1..20}-comment.json

Convert all of our comments to issues:

    find bugs -name 'bug*-comment.json' | xargs -n 1000 ./convert_comments.py

Get Bugzilla's "real name" field for each user:

    ./fetch_users.py

Create issues:

    ./create_issues.py --make-public wireshark/bz-migration-test 1-99 1500-15099

Once the project is complete, the aim is to use the routines from
`convert_comments.py` in another utility to convert bugs into a format suitable
for GitLab. Either by using the GitLab APIs directly, or by creating a project
that can be imported.
