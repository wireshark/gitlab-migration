# Notes
Random notes on migrating Bugzilla entries to GitLab issues.

API docs: <https://bugzilla.readthedocs.io/en/5.0/api/>

<https://bugs.wireshark.org/bugzilla/editusers.cgi?action=list&grouprestrict=1&groupid=8&is_enabled=1>
```javascript
void prompt('', JSON.stringify(Array.from($("#admin_table").rows).slice(1).filter((x) => /\S+/.test(x.cells[2].textContent)).map((x) => [x.cells[0].firstElementChild.href.match(/\d+$/)[0], x.cells[0].textContent.replace(/\s+/g, ''), x.cells[2].textContent.replace(/\s+/g, '')])))
```

Example for querying a private bug with an API key:
`https://bugs.wireshark.org/bugzilla/rest/bug/7697?api_key=...`

Dumb way to retrieve public bugs, superseded by `./fetch_bugs.py --ids 1-100`
```sh
for i in {1..100}; do test -s bug$i.json || curl -fs https://bugs.wireshark.org/bugzilla/rest/bug/$i | jq -S > bug$i.json; done
```

<https://gitlab.gnome.org/federico/bugzilla-to-gitlab-migrator/-/blob/master/bztogl.py>
<https://mail.gnome.org/archives/desktop-devel-list/2018-March/msg00023.html>

https://docs.gitlab.com/ee/api/issues.html#new-issue
```
POST /projects/:id/issues
title           string
description     string
confidential    boolean
labels          string  comma-separated label names
created_at      string  ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z (requires admin or project/group owner rights)
```

<https://docs.gitlab.com/ee/api/notes.html#create-new-issue-note>
```
POST /projects/:id/issues/:issue_iid/notes
body            string
created_at      string  ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z (requires admin or project/group owner rights)
```

<https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#default-closing-pattern>

Finding users by email
<https://gitlab.com/api/v4/users?search=peter@lekensteyn.nl>
`response[].username` (and `.id`/`.name` for future reference?)

Stats (see <https://gitlab.com/wireshark/gitlab-migration/-/wikis/Bug-Reports-And-Issue-Tracking-%28Bugzilla%29>):
- Highest Bug ID: 16500
- Highest Comment ID: 96856

Formatting requirements for comment:
- Convert bug references to links (`Bug 1234`, `https://bugs.wireshark.org/...`).
  Bugzilla autolinks bugs, comments, and attachments. See
  https://bugzilla.readthedocs.io/en/5.0/using/tips.html#autolinkification
  and
  https://github.com/bugzilla/bugzilla/blob/5.0/Bugzilla/Template.pm#L147
- Convert commit identifiers to links (automatically by GitLab).
- Escape other characters such as `#` and `<`. Stops replies such as
  `(In reply to Foo Bar from comment #4)`

Notable bugs and comments:
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=11#c0
  Preformatted text, no tabs.
  Multiple dashes (H2)
  Escaped '\n's
  Canary for inadvertent "PKCS #11" autolinking
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=38
  Preformatted text with tabs
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=7214
  Old-style version info.
  Attachment description contains special charaters.
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=13417
  Attachments 15252 and 15253 are empty.
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=13881
  Attachments
  Lots of comments
  Comment 301 is hairily-formatted
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=14994
  Code (stack trace) that passes sanitization.
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=15000
  Duplicate
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=15006
  Build information has no trailing newlines
  "In reply to" Markdown
  "In reply to" blockquote
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=15035
  Private bug
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=15021
  Nested "In reply to"
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=15022
  Crash
  External "See also" link
  https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=15033
  Bugzilla "See also" link
  Build information has trailing newlines
  Email fallback
  Enhancement
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=15051
  Windows path (C:\buildbot\...)
- https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=15961
  Spam comment

Questions:
- How many labels do we want?
  I (Gerald) am leaning toward fewer, more general ones instead of mapping all of Bugzilla's attributes.
- How should we format commenter names?
  We get the commenter's email address from Bugzilla and currently show the username portion.

Workflow:
. Create a migration user.
. Retrieve bug info.
. Retrieve comments for bug.
. Retrieve attachments.
. Retrieve users.
. Create labels.
. Upload attachments.
. Convert bugs+comments+attachments to issues+notes+attachments.
. Upload issues+notes.