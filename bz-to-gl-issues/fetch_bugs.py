#!/usr/bin/env python3
import argparse
import concurrent.futures
import datetime
import json
import logging
import os
import os.path
import time

import requests


this_dir = os.path.dirname(__file__)
private_bug = {}
private_comment = {}


def parse_bugids(ids):
    bugids = set()
    for val in ids:
        try:
            start = int(val)
            stop = start
        except ValueError:
            start, stop = tuple(map(int, val.split('-')))
        bugids.update(range(start, stop+1))
    return sorted(bugids)


def fetch_bug(session, bzurl, bugid):
    r = session.get(f'{bzurl}/rest/bug/{bugid}')
    obj = r.json()
    if r.status_code != 200:
        if obj['code'] == 102:
            pb = {}
            pb.update(private_bug)
            pb['bugs'][0]['id'] = bugid
            return pb
        logging.warning('failed to retrieve bug %d: %s',
                        bugid, obj.get('message'))
        return

    # We can't determine the time for private bugs, so just use the previous bug's creation time.
    prev_bug_time = obj['bugs'][0]['creation_time']
    private_bug['bugs'][0]['creation_time'] = prev_bug_time
    private_bug['bugs'][0]['last_change_time'] = prev_bug_time
    private_comment['creation_time'] = prev_bug_time
    private_comment['time'] = prev_bug_time
    return obj


def fetch_comments(session, bzurl, bugid):
    r = session.get(f'{bzurl}/rest/bug/{bugid}/comment')
    obj = r.json()
    if r.status_code != 200:
        if obj['code'] == 102:
            comment = {}
            comment.update(private_comment)
            comment['bug_id'] = bugid
            logging.warning(f'Bug {bugid} is private')
            return { "bugs": { f'{bugid}': { 'comments': [ comment ] } } }
        logging.warning('failed to retrieve comments for bug %d: %s',
                        bugid, obj.get('message'))
        return
    return obj


def fetch_and_save_file(bug_dir, what, session, bzurl, bugid):
    if what == 'bug':
        filename = f'bug{bugid}.json'
        label = f'bug {bugid}'
    elif what == 'comments':
        filename = f'bug{bugid}-comment.json'
        label = f'comments for bug {bugid}'
    else:
        raise AssertionError
    filename = os.path.join(bug_dir, filename)
    try:
        size = os.path.getsize(filename)
    except FileNotFoundError:
        size = None
    if size:
        logging.info('%s already retrieved, %s bytes', label, size)
        return True
    starttime = time.monotonic()
    if what == 'bug':
        obj = fetch_bug(session, bzurl, bugid)
    elif what == 'comments':
        obj = fetch_comments(session, bzurl, bugid)
    endtime = time.monotonic()
    if not obj:
        return False
    with open(filename, 'w') as f:
        json.dump(obj, f, indent=2, sort_keys=True)
        f.write('\n')
    logging.info('Retrieved %s, took %.3fs', label, endtime - starttime)
    return True


parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true',
                    help='Enable debug logging')
parser.add_argument('--bzurl', default='https://bugs.wireshark.org/bugzilla/',
                    help='Bugzilla base URL (default: %(default)s)')
parser.add_argument('--bug-dir', default=os.path.join(this_dir, 'bugs'),
                    help='Directory in which to place bugs and comments (default: <script_dir>/bugs)')
parser.add_argument('--threads', type=int, default=0,
                    help='Number of threads to use.')
parser.add_argument('ids', nargs='+', default=[],
                    help='Bug ID or Bug ID ranges, e.g. 1 or 1-9')


def fetch_and_save_bug_and_comments(bug_dir, session, bzurl, bugid):
    bug_ok = fetch_and_save_file(bug_dir, 'bug', session, bzurl, bugid)
    comments_ok = False
    if bug_ok:
        comments_ok = fetch_and_save_file(bug_dir, 'comments', session, bzurl, bugid)
    return bug_ok, comments_ok


def do_fetching(bug_dir, bzurl, bugids, ok_bugs, failed_bugs, ok_comments, failed_comments):
    session = requests.Session()
    while True:
        try:
            bugid = bugids.pop(0)
        except IndexError:
            break
        bug_ok, comments_ok = fetch_and_save_bug_and_comments(bug_dir, session, bzurl, bugid)
        ok_bugs.append(bugid) if bug_ok else failed_bugs.append(bugid)
        ok_comments.append(bugid) if comments_ok else failed_bugs.append(bugid)


def main():
    args = parser.parse_args()
    logging.basicConfig(format='%(asctime)s %(message)s',
                        level=logging.DEBUG if args.debug else logging.INFO)

    bzurl = args.bzurl
    bug_dir = args.bug_dir
    threads = args.threads
    bugids = parse_bugids(args.ids)

    if not bugids:
        logging.warning('No bugs selected')
        return
    logging.info('Selected %d bugs', len(bugids))

    fallback_time = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime())
    with open(os.path.join(this_dir, 'private-bug.json'), 'r') as pb_f:
        pb_info = json.load(pb_f)
        pb_info['bugs'][0]['creation_time'] = fallback_time
        pb_info['bugs'][0]['last_change_time'] = fallback_time
        private_bug.update(pb_info)

    with open(os.path.join(this_dir, 'private-comment.json'), 'r') as pc_f:
        pc_info = json.load(pc_f)
        pc_info['bugs']['0']['comments'][0]['creation_time'] = fallback_time
        pc_info['bugs']['0']['comments'][0]['time'] = fallback_time
        private_comment.update(pc_info['bugs']['0']['comments'][0])

    starttime = time.monotonic()

    ok_bugs = []
    failed_bugs = []
    ok_comments = []
    failed_comments = []
    if threads > 0:
        with concurrent.futures.ThreadPoolExecutor(max_workers=threads) as executor:
            for _ in range(threads):
                executor.submit(do_fetching, bug_dir, bzurl, bugids, ok_bugs, failed_bugs, ok_comments, failed_comments)
    else:
        do_fetching(bug_dir, bzurl, bugids, ok_bugs, failed_bugs, ok_comments, failed_comments)

    endtime = time.monotonic()
    logging.info(f'Done fetching {len(ok_bugs)} bugs and {len(ok_comments)} comments. Took {datetime.timedelta(seconds=endtime-starttime)}.')
    if failed_bugs:
        logging.warning('Failed to fetch the following bugs: ' + ', '.join(sorted(failed_bugs)))
    if failed_comments:
        logging.warning('Failed to fetch the following comments: ' + ', '.join(sorted(failed_comments)))

if __name__ == '__main__':
    main()
