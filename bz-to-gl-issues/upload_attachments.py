#!/usr/bin/env python3
import argparse
import datetime
import gitlab
import json
import logging
import os
import os.path
import re
import time

# this_dir = os.path.dirname(__file__)
bz_attach_re = re.compile(r'^bug(?P<bug_id>\d+)-attachment(?P<attach_id>\d+)-(?P<upload_name>.+)', re.ASCII)

# https://docs.gitlab.com/ee/api/projects.html#upload-a-file

parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true',
                    help='Enable debug logging')
parser.add_argument('--pgsection', default='gitlab',
                    help='~/.python-gitlab.cfg section (default: %(default)s)')
parser.add_argument('--force', action='store_true',
                    help='Re-upload and overwrite existing attachmentXYZ-upload.json files')
parser.add_argument('project_path',
                    help='Project path, e.g. wireshark/wireshark')
parser.add_argument('attachment', nargs='+',
                    help='One or more Bugzilla attachments. File names must be of the form bug<number>-attachment<number>-<name>')


def upload_attachment(gl_project, bz_attachment_file, force_upload):
    dirname = os.path.dirname(bz_attachment_file)
    basename = os.path.basename(bz_attachment_file)

    m = bz_attach_re.match(basename)
    if not m:
        logging.warning(f'Invalid attachment filename: {bz_attachment_file}')
        return
    bz_attach_id = m.group('attach_id')
    upload_info = {'bz_attach_id': bz_attach_id}
    json_name = os.path.join(dirname, f'attachment{bz_attach_id}-upload.json')
    if os.path.isfile(json_name) and not force_upload:
        logging.info(f'Skipping upload of attachment {bz_attach_id}: {json_name} exists')
        return

    res = gl_project.upload(m.group('upload_name'), filepath=bz_attachment_file, retry_transient_errors=True)
    upload_info.update(res)
    with open(json_name, 'w') as jf:
        json.dump(upload_info, jf, indent=2, sort_keys=True)
        jf.write('\n')
    logging.info(f'Uploaded attachment {bz_attach_id} as {upload_info["url"]}')


def main():
    args = parser.parse_args()
    logging.basicConfig(format='%(asctime)s %(message)s',
                        level=logging.DEBUG if args.debug else logging.INFO)

    gl = gitlab.Gitlab.from_config(args.pgsection)
    gl.auth()

    gl_project = gl.projects.get(args.project_path, retry_transient_errors=True)

    starttime = time.monotonic()

    for bz_attachment_file in args.attachment:
        upload_attachment(gl_project, bz_attachment_file, args.force)

    endtime = time.monotonic()
    logging.info(f'Done uploading. Took {datetime.timedelta(seconds=endtime-starttime)}.')

if __name__ == '__main__':
    main()
