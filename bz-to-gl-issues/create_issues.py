#!/usr/bin/env python3
import argparse
import concurrent.futures
import datetime
import gitlab
import json
import logging
import os
import re
import time

this_dir = os.path.dirname(__file__)
bz_url = 'https://bugs.wireshark.org/bugzilla'
bz_url_to_id_re = re.compile('https?://bugs.wireshark.org/bugzilla/show_bug.cgi.id=(?P<bug_id>\d+)')
bug_users = {} # { 'email@addr.ess': { real_name': 'A. Name', 'fetched': True }
issue_notes = {} # { iid: {'attachment_id': ..., } }

# https://docs.gitlab.com/ee/api/issues.html

def parse_bugids(ids):
    bugids = set()
    for val in ids:
        try:
            start = int(val)
            stop = start
        except ValueError:
            start, stop = tuple(map(int, val.split('-')))
        bugids.update(range(start, stop+1))
    return sorted(bugids)


def init_issue_users(bug_dir):
    with open(os.path.join(bug_dir, 'bug-users.json'), 'r') as bu_f:
        bug_users.update(json.load(bu_f))

    logging.info(f'Loaded {len(bug_users)} users.')


bz_prod_cmp_to_label = {} # { "product:component": "label", "product": "label" }
bz_op_sys_to_label = {} # { op_sys regex: label }
bz_version_to_label = {} # { version regex: label }
def init_issue_labels():
    with open(os.path.join(this_dir, 'labels.json'), 'r') as lj_f:
        labels = json.load(lj_f)
        for label in labels:
            try:
                mapping = {f'{label["bz_product"]}:{label["bz_component"]}': label['name']}
                bz_prod_cmp_to_label.update(mapping)
            except KeyError:
                try:
                    mapping = {f'{label["bz_product"]}': label['name']}
                    bz_prod_cmp_to_label.update(mapping)
                except KeyError:
                    pass

            try:
                mapping = {re.compile(label["bz_op_sys_re"], re.IGNORECASE): label['name']}
                bz_op_sys_to_label.update(mapping)
            except KeyError:
                pass

            try:
                mapping = {re.compile(label["bz_version_re"], re.IGNORECASE): label['name']}
                bz_version_to_label.update(mapping)
            except KeyError:
                pass

    logging.info(f'Loaded {len(labels)} labels.')


crash_bug_re = re.compile('(crash|fuzz|segfault|segmentation.fault|loop)', re.IGNORECASE)
crash_bug_severities = ["Major", "Critical", "Blocker"]
def get_issue_labels(bug_attrs):
    labels = []

    try:
        labels.append(bz_prod_cmp_to_label[f'{bug_attrs["product"]}:{bug_attrs["component"]}'])
    except KeyError:
        try:
            labels.append(bz_prod_cmp_to_label[f'{bug_attrs["product"]}'])
        except KeyError:
            pass

    for os_re, label in bz_op_sys_to_label.items():
        if os_re.match(bug_attrs["op_sys"]):
            labels.append(label)
            break

    for version_re, label in bz_version_to_label.items():
        if version_re.match(bug_attrs["version"]):
            labels.append(label)
            break

    if bug_attrs["severity"] in crash_bug_severities and crash_bug_re.search(bug_attrs["summary"]):
        labels.append("crash")

    if bug_attrs["severity"] == "Enhancement":
        labels.append("enhancement")

    return ','.join(labels)


def get_bug_attrs(bug_dir, bugid):
    bug_filename = os.path.join(bug_dir, f'bug{bugid}.json')
    full_bug_attrs = None
    try:
        with open(bug_filename, 'r') as bj_f:
            full_bug_attrs = json.load(bj_f)
    except:
        logging.warning(f'Unable to load {bug_filename}')
        return None

    if not full_bug_attrs:
        logging.warning(f'No attributes for {bugid}')

    # bugXXX.json:
    # {
    #   "bugs": [
    #     {
    #       "alias": [],
    try:
        return full_bug_attrs['bugs'][0]
    except:
        logging.warning(f'Malformed attributes for {bugid}')
        return None


def init_issue_notes(note_dir, bugids):
    for issue_id in bugids:
        with open(os.path.join(note_dir, f'issue{issue_id}-notes.json'), 'r') as in_f:
            issue_notes[issue_id] = json.load(in_f)

    logging.info(f'Loaded {len(issue_notes)} notes.')


bug_attachment_re = re.compile('^bug(?P<bug_id>\d+)-attachment(?P<attachment_id>\d+)\.json$', re.ASCII)
issue_attachments = {} # { iid: [{'bz_attach_id': ..., }, ] }
# Special GitLab references / autolinks. Copied from sanitize() in convert_comments.py.
# https://docs.gitlab.com/ee/user/markdown.html#special-gitlab-references
special_reference_re = re.compile('([@#!\$&~%])(\w)')
def init_issue_attachments(attachment_dir):
    attachment_count = 0
    for dirent in os.listdir(attachment_dir):
        m = bug_attachment_re.match(dirent)
        if not m:
            continue
        issue_id = int(m.group('bug_id'))
        attachment_id = int(m.group('attachment_id'))
        with open(os.path.join(this_dir, attachment_dir, dirent), 'r') as ba_f:
            bz_attachment = json.load(ba_f)
        try:
            with open(os.path.join(this_dir, attachment_dir, f'attachment{attachment_id}-upload.json'), 'r') as au_f:
                attachment_upload = json.load(au_f)
                attachment_upload['bz_summary'] = special_reference_re.sub(r'\1&#8205;\2', bz_attachment['summary'])
                try:
                    issue_attachments[issue_id].append(attachment_upload)
                except KeyError:
                    issue_attachments[issue_id] = [attachment_upload]
                attachment_count += 1
        except FileNotFoundError:
            logging.warning(f'No attachment file found for {dirent}.')

    if not issue_attachments:
        logging.warning(f'No attachments found in {attachment_dir}.')

    logging.info(f'Loaded {attachment_count} attachments for {len(issue_attachments)} issues.')

def get_full_user_name(name):
    real_name = ''
    if name in bug_users:
        real_name = bug_users[name]['real_name'].strip()
    # Modified from sanitize() in convert_comments.py
    # Identify dangerous markdown characters and escape them.
    # https://spec.commonmark.org/0.29/#ascii-punctuation-character
    badchars = r'!"#$%&' + r"()*+,/:;<=>?[\]^_`{|}~"
    trans = str.maketrans({k: '\\' + k for k in badchars})
    s_user, s_domain = name.translate(trans).split('@')
    s_real_name = real_name.translate(trans).strip()

    if s_real_name:
        return s_real_name

    return f'{s_user[:3]}…@&#8205;{s_domain[:3]}…'.strip()


public_issue_description_template = '''\
**This issue was migrated from [bug {id}]({bz_url}/show_bug.cgi?id={id}) in our old bug tracker.**

Original bug information:

**Reporter:** {creator_full_user_name}\\
**Status:** {status} {resolution}\\
**Product:** {product}\\
**Component:** {component}\\
**OS:** {op_sys}\\
**Platform:** {platform}\\
**Version:** {version}
'''
private_issue_description_template = '''\
We were unable to migrate [bug {id}]({bz_url}/show_bug.cgi?id={id}) from our old bug tracker.
'''
sa_bug_re = re.compile(f'bug(?P<bug_id>\d+)-attachment(?P<attachment_id>\d+).json')
def get_issue_description(bug_attrs):
    issue_attrs = bug_attrs
    issue_attrs['bz_url'] = bz_url
    issue_attrs['creator_full_user_name'] = get_full_user_name(bug_attrs['creator'])

    if 'ws_bug_is_private' in bug_attrs and bug_attrs['ws_bug_is_private']:
        return private_issue_description_template.format(**issue_attrs), 0

    issue_description = public_issue_description_template.format(**issue_attrs)

    attachment_count = 0
    try:
        attachments = sorted(issue_attachments[issue_attrs['id']], key=lambda k: k['bz_attach_id'])
        issue_description += '\nAttachments:\n\n'
        for attachment in attachments:
            issue_description += f'{attachment["markdown"]}: {attachment["bz_summary"]}  \n'
            attachment_count += 1
    except KeyError:
        pass

    if bug_attrs['dupe_of']:
        issue_description += f'\nDuplicate of issue #{bug_attrs["dupe_of"]}\n'

    if bug_attrs['see_also']:
        issue_description += '\nSee also:\n'
        for sa_url in bug_attrs['see_also']:
            m = bz_url_to_id_re.match(sa_url)
            if m:
                issue_id = m.group('bug_id')
                issue_description += f'Issue #{issue_id}  \n'
            else:
                issue_description += f'{sa_url}  \n'

    return issue_description, attachment_count


def plurality(v):
    if hasattr(type(v), '__iter__'):
        count = len(v)
    else:
        count = v
    if count == 1:
        return ''
    return 's'


def create_issue(gl_project, bug_attrs):
    # If we have an existing issue, delete it in order to clear out any
    # existing modification history and notes.
    action = 'Created'
    try:
        issue = gl_project.issues.get(bug_attrs["id"], retry_transient_errors=True)
        issue.delete(retry_transient_errors=True)
        action = 'Deleted+Created'
    except:
        pass

    starttime = time.monotonic()

    description, attachment_count = get_issue_description(bug_attrs)
    issue_attrs = {
        'iid': bug_attrs["id"],
        'title': bug_attrs['summary'],
        'description': description,
        'labels': get_issue_labels(bug_attrs),
        'created_at': bug_attrs['creation_time'],
        'updated_at': bug_attrs['last_change_time'],
    }
    issue = gl_project.issues.create(issue_attrs, retry_transient_errors=True)

    status = 'open'
    # if bug_attrs["status"] == "RESOLVED":
    if bug_attrs["resolution"] != "":
        status = 'closed'
        issue.state_event = 'close'
        issue.updated_at = bug_attrs['last_change_time']
        issue.save(retry_transient_errors=True)

    endtime = time.monotonic()
    logging.info(f'{action} {status} issue {issue.iid} from bug {bug_attrs["id"]}, {attachment_count} attachment{plurality(attachment_count)}, took {endtime-starttime:.3f}s.')

    try:
        notes = issue_notes[issue.iid]
    except KeyError:
        notes = []

    starttime = time.monotonic()

    for note in notes:
        full_user_name = get_full_user_name(note['bz_creator'])
        note_attrs = {
            # https://gitlab.gnome.org/GNOME/glib/-/issues/2
            'body': f':speech_balloon: **{full_user_name}** said:\n\n{note["body"]}',
            'created_at': note['bz_creation_time']
        }
        issue.notes.create(note_attrs, retry_transient_errors=True)

    if notes:
        endtime = time.monotonic()
        logging.info(f'Added {len(notes)} note{plurality(notes)} to issue {issue.iid}, took {endtime-starttime:.3f}s.')

    return True, len(notes)

parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true',
                    help='Enable debug logging')
parser.add_argument('--pgsection', default='gitlab',
                    help='~/.python-gitlab.cfg section (default: %(default)s)')
parser.add_argument('--bug-dir', default=os.path.join(this_dir, 'bugs'),
                    help='Directory containing the output of fetch_bugs.py (default: <script_dir>/bugs)')
parser.add_argument('--attachment-dir', default=os.path.join(this_dir, 'attachments'),
                    help='Directory containing the output of upload_attachments.py (default: <script_dir>/attachments)')
parser.add_argument('--note-dir', default=os.path.join(this_dir, 'notes'),
                    help='Directory containing the output of fetch_bugs.py (default: <script_dir>/notes)')
parser.add_argument('--threads', type=int, default=0,
                    help='Number of threads to use.')
parser.add_argument('--make-public', action='store_true',
                    help='Make project public when done')
parser.add_argument('project_path',
                    help='Project path, e.g. wireshark/wireshark')
parser.add_argument('ids', nargs='+', default=[],
                    help='Bug ID or Bug ID ranges, e.g. 1 or 1-9')


def do_creating(bug_dir, pgsection, project_path, bugids):
    gl = gitlab.Gitlab.from_config(pgsection)
    gl.auth()
    gl_project = gl.projects.get(project_path, retry_transient_errors=True)
    issue_count = 0
    note_count = 0
    while True:
        try:
            bugid = bugids.pop(0)
        except IndexError:
            break
        bug_attrs = get_bug_attrs(bug_dir, bugid)
        if not bug_attrs:
            logging.warning(f'No attributes for {bugid}')
            failed_issues.append(bugid)
            failed_notes.append(bugid)
            continue
        issue_ok, notes_created = create_issue(gl_project, bug_attrs)
        if issue_ok:
            issue_count += 1
            note_count += notes_created
        # else:
        #     logging.warning(f'Creating {bugid} and its notes failed. Retrying.')
        #     time.sleep(1.0) # In case the failure is due to rate limiting.
        #     bugids.append(bugid)

    return issue_count, note_count


def main():
    args = parser.parse_args()
    logging.basicConfig(format='%(asctime)s %(message)s',
                        level=logging.DEBUG if args.debug else logging.INFO)

    bug_dir = args.bug_dir
    attachment_dir = args.attachment_dir
    note_dir = args.note_dir
    threads = args.threads
    bugids = parse_bugids(args.ids)

    if not bugids:
        logging.warning('No bugs selected')
        return
    logging.info('Selected %d bugs', len(bugids))

    init_issue_users(bug_dir)
    init_issue_labels()
    init_issue_attachments(attachment_dir)
    init_issue_notes(note_dir, bugids)

    gl = gitlab.Gitlab.from_config(args.pgsection)
    gl.auth()
    gl_project = gl.projects.get(args.project_path, retry_transient_errors=True)

    starttime = time.monotonic()

    # If we're not private we're prone to getting:
    # gitlab.exceptions.GitlabHttpError: 400: {'base': ['Your issue has been recognized as spam. Please, change the content or solve the reCAPTCHA to proceed.']}
    make_public = args.make_public
    if gl_project.visibility == 'public':
        make_public = True
        gl_project.visibility = 'private'
        gl_project.save(retry_transient_errors=True)
        logging.warning(f'Switched repository {gl_project.path_with_namespace} from public to private.')

    issue_count = 0
    note_count = 0
    if threads > 0:
        with concurrent.futures.ThreadPoolExecutor(max_workers=threads) as executor:
            futures = []
            for _ in range(threads):
                futures.append(executor.submit(do_creating, bug_dir, args.pgsection, args.project_path, bugids))
            for future in concurrent.futures.as_completed(futures):
                    try:
                        f_issues, f_notes = future.result()
                        issue_count += f_issues
                        note_count += f_notes
                    except Exception as exc:
                        print(f'Executor generated an exception: {exc}')
                        raise

    else:
        issue_count, note_count = do_creating(bug_dir, args.pgsection, args.project_path, bugids)

    if make_public:
        gl_project.visibility = 'public'
        gl_project.save(retry_transient_errors=True)
        logging.warning(f'Switched repository {gl_project.path_with_namespace} from private to public.')

    endtime = time.monotonic()
    logging.info(f'Created {issue_count} issue{plurality(issue_count)} and {note_count} note{plurality(note_count)}, took {datetime.timedelta(seconds=endtime-starttime)}.')

if __name__ == '__main__':
    main()
